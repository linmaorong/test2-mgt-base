import 'element-ui/lib/theme-chalk/index.css' // 字体icon 勿删
import Vue from 'vue'
import nutMgt from '@beexiao/nut-mgt'
import App from './App.vue'
import router from './router'
import store from './store'
Vue.use(nutMgt)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
