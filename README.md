# test2-mgt-base (vue-cli 3.0项目)
```
 bee-nut-cli工具使用的模板 里边自带element跟nut-mgt组件
```

## Project setup
```
yarn setup
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
