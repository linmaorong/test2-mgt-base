const path = require('path')
let publicPath = '/test2-mgt-base'
if (process.env.NODE_ENV === 'production') {
  publicPath = '//s.vipkidstatic.com/beeschool/test2-mgt-base/static'
}
module.exports = {
  // 项目部署基础
  // 默认情况下，我们假设你的应用将被部署在域的根目录下,
  // 例如：https://www.my-app.com/
  // 如果您的应用程序部署在子路径中，则需要在这指定子路径
  // 例如：https://www.foobar.com/my-app/
  // 需要将它改为'/my-app/'
  publicPath,

  // 输出构建的文件的地方
  outputDir: 'output',

  // 是否为保存的lint使用eslint-loader
  // 可选值： true | false | 'error'
  // 当设置为“error”时，lint错误会导致编译打包失败
  lintOnSave: true,

  // 调整内部webpack配置
  chainWebpack: config => {
    const types = ["vue-modules", "vue", "normal-modules", "normal"];
    types.forEach(type =>
      addStyleResource(config.module.rule("less").oneOf(type)) // 配置自己的less变量跟函数
    );
  },
  configureWebpack: () => {},

  // 为生产构建生成sourceMap？
  productionSourceMap: false,

  // 配置webpack-dev-server行为
  devServer: {
    open: false,
    host: '0.0.0.0',
    port: 8080,
    https: false,
    hotOnly: false,
    // 参阅 https://github.com/vuejs/vue-cli/blob/dev/docs/cli-service.md#configuring-proxy
    proxy: { // 代理 自己配置
      '/api/gw': {
        target: 'http://test7-mgt.vipfengxiao.com',
        changeOrigin: true
      }
    },
    before: app => {}
  },

  // 第三方插件的选项
  pluginOptions: {
    // ...
  }
}
function addStyleResource(rule) {
  rule
    .use("style-resource")
    .loader("style-resources-loader")
    .options({
      patterns: [path.resolve(__dirname, "./my-theme/index.less")]
    });
}

